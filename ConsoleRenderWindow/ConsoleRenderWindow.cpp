#include "ConsoleRenderWindow.h"

using namespace std;

ConsoleRenderWindow::ConsoleRenderWindow(int width, int height)
{
	m_windowWidth = width + 1;
	m_windowHeight = height;
}

void ConsoleRenderWindow::Initialise()
{
	_COORD coord;
	coord.X = m_windowWidth;
	coord.Y = m_windowHeight;

	_SMALL_RECT Rect;
	Rect.Top = 0;
	Rect.Left = 0;
	Rect.Bottom = 900 - 1;
	Rect.Right = 1600 - 1;

	HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);      // Get Handle 
	SetConsoleScreenBufferSize(Handle, coord);            // Set Buffer Size 
	SetConsoleWindowInfo(Handle, TRUE, &Rect);            // Set Window Size 



	//setup char* for rendering
	m_renderBuffer = new char[m_windowHeight * (m_windowWidth + 1)];
	ClearBuffer(m_renderBuffer);
	m_backBuffer = new char[m_windowHeight * (m_windowWidth + 1)];
	ClearBuffer(m_backBuffer);
}

void ConsoleRenderWindow::ClearBuffer(char* buffer)
{
	for (int y = 0; y < m_windowHeight; y++)
	{
		for (int x = 0; x < m_windowWidth; x++)
		{
			buffer[(y * m_windowWidth) + x] = ' ';
		}
	}

	for (int y = 0; y < m_windowHeight; y++)
	{
		if (y == m_windowHeight - 1) buffer[(y * m_windowWidth) + m_windowWidth - 1] = '\0';
		buffer[(y * m_windowWidth) + m_windowWidth - 1] = '\n';
	}
}

void ConsoleRenderWindow::SetXY(int x, int y, char fillChar)
{
	//COORD c = { x, y };
	//SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	if (x < m_windowWidth && y < m_windowHeight)
	{
		if (!bufferSwap)
		{
			m_renderBuffer[(y * m_windowWidth) + x] = fillChar;
		}
		else
		{
			m_backBuffer[(y * m_windowWidth) + x] = fillChar;
		}		
	}


}

void ConsoleRenderWindow::Write(int x, int y, char* charArray)
{
	string string = charArray;
	for (unsigned int i = 0; i < string.size(); i++)
	{
		SetXY(x + i, y, string[i]);
	}
}

void ConsoleRenderWindow::Write(int x, int y, char character)
{
	SetXY(x, y, character);
}

void ConsoleRenderWindow::Write(int x, int y, void* address)
{
	const void * hexAddress = static_cast<const void*>(address);
	std::stringstream ss;
	ss << "0x";
	ss << hexAddress;
	std::string string = ss.str();

	for (unsigned int i = 0; i < string.size() - 1; i++)
	{
		SetXY(x + i, y, string[i]);
	}
}


void ConsoleRenderWindow::DrawRect(int x, int y, int width, int height, bool fill, char fillChar)
{
	if (fill)
	{
		for (int xPos = x; xPos < x + width; xPos++)
		{
			for (int yPos = y; yPos < y + height; yPos++)
			{
				SetXY(xPos, yPos, fillChar);
			}
		}
	}
	else
	{
		SetXY(x, y, '+');

		for (int i = 1; i < width; i++) SetXY(i, 0, '-');
		SetXY(width, 0, '+');

		SetXY(x, height + y, '+');

		for (int i = 1; i < width; i++) SetXY(i, height, '-');
		SetXY(width, height, '+');

		for (int i = y + 1; i < height + y; i++)
		{
			SetXY(x, i, '|');
			SetXY(x + width, i, '|');
		}
	}
}

void ConsoleRenderWindow::Render()
{
	//clear screen
	system("cls");

	//draw buffer clear it then set swap
	if (bufferSwap)
	{
		printf("%s", m_renderBuffer);
		ClearBuffer(m_renderBuffer);
	}
	else
	{
		printf("%s", m_backBuffer);
		ClearBuffer(m_backBuffer);
	}
	bufferSwap = !bufferSwap;

	COORD coordScreen = { 0, 0 };
	SetConsoleCursorPosition(GetConsoleWindow(), coordScreen);
}