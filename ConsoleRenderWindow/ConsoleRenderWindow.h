#ifndef CONSOLERENDERWINDOW_H
#define CONSOLERENDERWINDOW_H

#include <iostream>

#include <Windows.h>
#include <string>
#include <sstream>

class ConsoleRenderWindow
{
private:
	int m_windowWidth, m_windowHeight;

	char* m_backBuffer;
	char* m_renderBuffer;
	bool bufferSwap = false;

	void SetXY(int x, int y, char fillChar);


	void ClearBuffer(char* buffer);

public:
	ConsoleRenderWindow(int width, int height);
	void Initialise();

	void DrawRect(int x, int y, int width, int height, bool fill, char fillChar);

	void Write(int x, int y, char* charArray);
	void Write(int x, int y, char character);
	void Write(int x, int y, void* memoryAddress);

	void Render();
};

#endif