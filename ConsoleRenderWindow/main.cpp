#include "ConsoleRenderWindow.h"

void main()
{
	ConsoleRenderWindow* window = new ConsoleRenderWindow(50, 50);
	window->Initialise();
	
	while (true)
	{
		window->DrawRect(0, 0, 30, 20, true, '*');
		window->Write(21, 21, "HELLO WORLD");
		window->Render();
		Sleep(60);
	}
}